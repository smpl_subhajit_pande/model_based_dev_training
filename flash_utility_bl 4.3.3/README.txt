"""
Copyright (C) 2019, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.

Module:         GEN3_FLASH_UTILITY
Date Created:   17th September 2020

Description: This module allows flashing/erasing compiled C code and calibration data partition via CAN interface to ISG GEN3 family ECUs from SEDEMAC
"""

Instructions:

To flash code -
1.Ensure a main.srec file in the directory. Replace file with new file if needed.
2.Click on the code_flash.bat file

To erase code -
1.Click on the code_erase.bat file

To erase calibration data in flash
1. Click on the calib_data_erase.bat file

To create a single srec file by merging srecs of bootloader and application -
1. Edit the merge_files.bat to give the address of the bootloader and application's srec files.
2. Save and close
3. Click on the merge_files.bat.
4. New file name.ecuimg file will be created.
5. Remove the ".ecuimg" part from the name.
