# SEDEMAC Proprietary License

Copyright (c) 2019, `SEDEMAC Mechatronics Pvt. Ltd`
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, is intended to be used only by authorized peronnels given written authorization for access by Chief Engineer, R&D or CTO. The software *SHOULD NOT* be used for personal projects of developers. The distribution is not permitted to be copied on any laptop/desktops outside of those managed by SEDEMAC IT infrastructure.


