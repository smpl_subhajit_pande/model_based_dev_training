<p align="center">
    <img width="400" height="200" src="docs/images/gen3_logo.jpg?raw=true">
</p>

# <center>ISG Gen3</center>

This project is for productizing BHspin + Phasing based ISG, with 12V based power assist as a significant add-on. The project is an undertaking by SEDEMAC SysE with support from R&D. The project implements the following:

1. **Cranking** (BHspin based motoring)
2. **Voltage regulation** (Bemf observer based generation)
3. **Forced motoring at higher speeds** (Bemf observer based motoring)

The following features are currently included but, intended to be removed for final product:
1. **Idle stability** (Bemf observer based motoring)
2. **Creep mode** (Bemf observer based motoring)
3. **Clutch plate speed matching** (Bemf observer based motoring)

The project is intended as a full fledged production ready application. This repository is intended for all development specific to ISG Gen3 on TVS Moped

## Contributors

The source code is currently being maintained by Vasu Sharma, Varun Gampa. The original project source is from Anay Joshi, Samyak Kamble and Neel Shah.