@echo off

if "%~1" == "" (
  SET _Mode=all
) ELSE (
  SET _Mode=%~1
)

IF "%_Mode%" == "CGU_NAME_LIST" (
echo *observer_bhspin2*
echo *LED_ERROR*
echo *observer_bhspin*
echo *Ramp*
echo *observer_bhspin_source_line*
echo *observer_bhspin_source*
echo *observer_hall*
echo *observer_bemf*
echo *motoring*
echo *observer_bemf_6Step*
echo *observer_bhspin_lib*
echo *MIL*
echo *PID*
echo *Gain_Scheduling*
echo *Controller*
GOTO :EOF
)

IF "%_Mode%" == "all" (

rem *********************************************************************************************
rem ** Cleanup all TargetLink predefined directories. 
rem *********************************************************************************************
if exist ".\TLProj" rmdir /s /q ".\TLProj"
if exist ".\CodeViewFiles" rmdir /s /q ".\CodeViewFiles"
if exist ".\CodeCoverageReport" rmdir /s /q ".\CodeCoverageReport"
if exist ".\doc" rmdir /s /q ".\doc"
if exist ".\TLSim" rmdir /s /q ".\TLSim"
if exist ".\TLBuild" rmdir /s /q ".\TLBuild"

rem *********************************************************************************************
rem ** Cleanup code generation unit specific directories. 
rem *********************************************************************************************
call:delete_observer_bhspin2_files
call:delete_led_error_files
call:delete_observer_bhspin_files
call:delete_ramp_files
call:delete_observer_bhspin_source_line_files
call:delete_observer_bhspin_source_files
call:delete_observer_hall_files
call:delete_observer_bemf_files
call:delete_motoring_files
call:delete_observer_bemf_6step_files
call:delete_observer_bhspin_lib_files
call:delete_mil_files
call:delete_pid_files
call:delete_gain_scheduling_files
call:delete_controller_files

rem *********************************************************************************************
rem ** cleanup ASAP2 files and files created during A2L file generation
rem *********************************************************************************************
if exist a2lexport.log                del a2lexport.log
if exist a2l_export_param.dd          del a2l_export_param.dd
if exist asap2_all_in_one.xsl         del asap2_all_in_one.xsl
if exist asap2_all_in_one_module.xsl  del asap2_all_in_one_module.xsl
if exist asap2_default.xsl            del asap2_default.xsl
if exist SymTabParser.cfg             del SymTabParser.cfg
if exist *.a2l                        del *.a2l

rem *********************************************************************************************
rem ** cleanup other files
rem *********************************************************************************************
if exist tl_autoscaling.log           del tl_autoscaling.log
if exist dsdd_validate.log            del dsdd_validate.log
if exist lnk*.tmp                     del lnk*.tmp
if exist make*.lck                    del make*.lck
if exist vc40.pdb                     del vc40.pdb
if exist vc50.pdb                     del vc50.pdb
if exist vc*.idb                      del vc*.idb
if exist NONE                         del NONE
if exist BS_Debug.txt                 del BS_Debug.txt
if exist hdi.hdc                      del hdi.hdc
if exist hdi.hds                      del hdi.hds
if exist tl_diff_cgopt_report.log     del tl_diff_cgopt_report.log

GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin2" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin2. 
rem *********************************************************************************************
call:delete_observer_bhspin2_files
GOTO :EOF
)

IF "%_Mode%" == "led_error" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit led_error. 
rem *********************************************************************************************
call:delete_led_error_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin. 
rem *********************************************************************************************
call:delete_observer_bhspin_files
GOTO :EOF
)

IF "%_Mode%" == "ramp" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit ramp. 
rem *********************************************************************************************
call:delete_ramp_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin_source_line" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin_source_line. 
rem *********************************************************************************************
call:delete_observer_bhspin_source_line_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin_source" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin_source. 
rem *********************************************************************************************
call:delete_observer_bhspin_source_files
GOTO :EOF
)

IF "%_Mode%" == "observer_hall" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_hall. 
rem *********************************************************************************************
call:delete_observer_hall_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bemf" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bemf. 
rem *********************************************************************************************
call:delete_observer_bemf_files
GOTO :EOF
)

IF "%_Mode%" == "motoring" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit motoring. 
rem *********************************************************************************************
call:delete_motoring_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bemf_6step" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bemf_6step. 
rem *********************************************************************************************
call:delete_observer_bemf_6step_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin_lib" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin_lib. 
rem *********************************************************************************************
call:delete_observer_bhspin_lib_files
GOTO :EOF
)

IF "%_Mode%" == "mil" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit mil. 
rem *********************************************************************************************
call:delete_mil_files
GOTO :EOF
)

IF "%_Mode%" == "pid" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit pid. 
rem *********************************************************************************************
call:delete_pid_files
GOTO :EOF
)

IF "%_Mode%" == "gain_scheduling" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit gain_scheduling. 
rem *********************************************************************************************
call:delete_gain_scheduling_files
GOTO :EOF
)

IF "%_Mode%" == "controller" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit controller. 
rem *********************************************************************************************
call:delete_controller_files
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin2
rem *********************************************************************************************


:delete_observer_bhspin2_files
set tl_subsys=observer_bhspin2
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin2_fri.c" del ".\TLSim\observer_bhspin2_fri.c" 
if exist ".\TLSim\observer_bhspin2_fri.h" del ".\TLSim\observer_bhspin2_fri.h" 
if exist ".\TLSim\observer_bhspin2_sf.c" del ".\TLSim\observer_bhspin2_sf.c" 
if exist ".\TLSim\observer_bhspin2_pcf.c" del ".\TLSim\observer_bhspin2_pcf.c" 
if exist ".\TLSim\observer_bhspin2_frm.h" del ".\TLSim\observer_bhspin2_frm.h" 
if exist ".\TLSim\globals_observer_bhspin2.*" del ".\TLSim\globals_observer_bhspin2.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin2\Metadata\observer_bhspin2_SubsystemObject.dd" del ".\TLProj\observer_bhspin2\Metadata\observer_bhspin2_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin2" rmdir /s /q ".\TLProj\observer_bhspin2" 
if exist ".\TLSim\StubCode\observer_bhspin2" rmdir /s /q ".\TLSim\StubCode\observer_bhspin2" 
if exist ".\TLProj\observer_bhspin2\CGReport" rmdir /s /q ".\TLProj\observer_bhspin2\CGReport" 
if exist ".\TLProj\observer_bhspin2\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin2\Metadata"
if exist ".\TLProj\observer_bhspin2" call:delete_if_empty ".\TLProj\observer_bhspin2"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit led_error
rem *********************************************************************************************


:delete_led_error_files
set tl_subsys=led_error
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\LED_ERROR_fri.c" del ".\TLSim\LED_ERROR_fri.c" 
if exist ".\TLSim\LED_ERROR_fri.h" del ".\TLSim\LED_ERROR_fri.h" 
if exist ".\TLSim\led_error_sf.c" del ".\TLSim\led_error_sf.c" 
if exist ".\TLSim\led_error_pcf.c" del ".\TLSim\led_error_pcf.c" 
if exist ".\TLSim\led_error_frm.h" del ".\TLSim\led_error_frm.h" 
if exist ".\TLSim\globals_led_error.*" del ".\TLSim\globals_led_error.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\LED_ERROR\Metadata\led_error_SubsystemObject.dd" del ".\TLProj\LED_ERROR\Metadata\led_error_SubsystemObject.dd" 
if exist ".\TLProj\LED_ERROR" rmdir /s /q ".\TLProj\LED_ERROR" 
if exist ".\TLSim\StubCode\LED_ERROR" rmdir /s /q ".\TLSim\StubCode\LED_ERROR" 
if exist ".\TLProj\LED_ERROR\CGReport" rmdir /s /q ".\TLProj\LED_ERROR\CGReport" 
if exist ".\TLProj\LED_ERROR\Metadata" call:delete_if_empty ".\TLProj\LED_ERROR\Metadata"
if exist ".\TLProj\LED_ERROR" call:delete_if_empty ".\TLProj\LED_ERROR"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin
rem *********************************************************************************************


:delete_observer_bhspin_files
set tl_subsys=observer_bhspin
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin_fri.c" del ".\TLSim\observer_bhspin_fri.c" 
if exist ".\TLSim\observer_bhspin_fri.h" del ".\TLSim\observer_bhspin_fri.h" 
if exist ".\TLSim\observer_bhspin_sf.c" del ".\TLSim\observer_bhspin_sf.c" 
if exist ".\TLSim\observer_bhspin_pcf.c" del ".\TLSim\observer_bhspin_pcf.c" 
if exist ".\TLSim\observer_bhspin_frm.h" del ".\TLSim\observer_bhspin_frm.h" 
if exist ".\TLSim\globals_observer_bhspin.*" del ".\TLSim\globals_observer_bhspin.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin\Metadata\observer_bhspin_SubsystemObject.dd" del ".\TLProj\observer_bhspin\Metadata\observer_bhspin_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin" rmdir /s /q ".\TLProj\observer_bhspin" 
if exist ".\TLSim\StubCode\observer_bhspin" rmdir /s /q ".\TLSim\StubCode\observer_bhspin" 
if exist ".\TLProj\observer_bhspin\CGReport" rmdir /s /q ".\TLProj\observer_bhspin\CGReport" 
if exist ".\TLProj\observer_bhspin\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin\Metadata"
if exist ".\TLProj\observer_bhspin" call:delete_if_empty ".\TLProj\observer_bhspin"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit ramp
rem *********************************************************************************************


:delete_ramp_files
set tl_subsys=ramp
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\Ramp_fri.c" del ".\TLSim\Ramp_fri.c" 
if exist ".\TLSim\Ramp_fri.h" del ".\TLSim\Ramp_fri.h" 
if exist ".\TLSim\ramp_sf.c" del ".\TLSim\ramp_sf.c" 
if exist ".\TLSim\ramp_pcf.c" del ".\TLSim\ramp_pcf.c" 
if exist ".\TLSim\ramp_frm.h" del ".\TLSim\ramp_frm.h" 
if exist ".\TLSim\globals_ramp.*" del ".\TLSim\globals_ramp.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\Ramp\Metadata\ramp_SubsystemObject.dd" del ".\TLProj\Ramp\Metadata\ramp_SubsystemObject.dd" 
if exist ".\TLProj\Ramp" rmdir /s /q ".\TLProj\Ramp" 
if exist ".\TLSim\StubCode\Ramp" rmdir /s /q ".\TLSim\StubCode\Ramp" 
if exist ".\TLProj\Ramp\CGReport" rmdir /s /q ".\TLProj\Ramp\CGReport" 
if exist ".\TLProj\Ramp\Metadata" call:delete_if_empty ".\TLProj\Ramp\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\Ramp" call:delete_if_empty ".\TLProj\Ramp"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin_source_line
rem *********************************************************************************************


:delete_observer_bhspin_source_line_files
set tl_subsys=observer_bhspin_source_line
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin_source_line_fri.c" del ".\TLSim\observer_bhspin_source_line_fri.c" 
if exist ".\TLSim\observer_bhspin_source_line_fri.h" del ".\TLSim\observer_bhspin_source_line_fri.h" 
if exist ".\TLSim\observer_bhspin_source_line_sf.c" del ".\TLSim\observer_bhspin_source_line_sf.c" 
if exist ".\TLSim\observer_bhspin_source_line_pcf.c" del ".\TLSim\observer_bhspin_source_line_pcf.c" 
if exist ".\TLSim\observer_bhspin_source_line_frm.h" del ".\TLSim\observer_bhspin_source_line_frm.h" 
if exist ".\TLSim\globals_observer_bhspin_source_line.*" del ".\TLSim\globals_observer_bhspin_source_line.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin_source_line\Metadata\observer_bhspin_source_line_SubsystemObject.dd" del ".\TLProj\observer_bhspin_source_line\Metadata\observer_bhspin_source_line_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin_source_line" rmdir /s /q ".\TLProj\observer_bhspin_source_line" 
if exist ".\TLSim\StubCode\observer_bhspin_source_line" rmdir /s /q ".\TLSim\StubCode\observer_bhspin_source_line" 
if exist ".\TLProj\observer_bhspin_source_line\CGReport" rmdir /s /q ".\TLProj\observer_bhspin_source_line\CGReport" 
if exist ".\TLProj\observer_bhspin_source_line\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin_source_line\Metadata"
if exist ".\TLProj\observer_bhspin_source_line" call:delete_if_empty ".\TLProj\observer_bhspin_source_line"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin_source
rem *********************************************************************************************


:delete_observer_bhspin_source_files
set tl_subsys=observer_bhspin_source
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin_source_fri.c" del ".\TLSim\observer_bhspin_source_fri.c" 
if exist ".\TLSim\observer_bhspin_source_fri.h" del ".\TLSim\observer_bhspin_source_fri.h" 
if exist ".\TLSim\observer_bhspin_source_sf.c" del ".\TLSim\observer_bhspin_source_sf.c" 
if exist ".\TLSim\observer_bhspin_source_pcf.c" del ".\TLSim\observer_bhspin_source_pcf.c" 
if exist ".\TLSim\observer_bhspin_source_frm.h" del ".\TLSim\observer_bhspin_source_frm.h" 
if exist ".\TLSim\globals_observer_bhspin_source.*" del ".\TLSim\globals_observer_bhspin_source.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin_source\Metadata\observer_bhspin_source_SubsystemObject.dd" del ".\TLProj\observer_bhspin_source\Metadata\observer_bhspin_source_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin_source" rmdir /s /q ".\TLProj\observer_bhspin_source" 
if exist ".\TLSim\StubCode\observer_bhspin_source" rmdir /s /q ".\TLSim\StubCode\observer_bhspin_source" 
if exist ".\TLProj\observer_bhspin_source\CGReport" rmdir /s /q ".\TLProj\observer_bhspin_source\CGReport" 
if exist ".\TLProj\observer_bhspin_source\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin_source\Metadata"
if exist ".\TLProj\observer_bhspin_source" call:delete_if_empty ".\TLProj\observer_bhspin_source"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_hall
rem *********************************************************************************************


:delete_observer_hall_files
set tl_subsys=observer_hall
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_hall_fri.c" del ".\TLSim\observer_hall_fri.c" 
if exist ".\TLSim\observer_hall_fri.h" del ".\TLSim\observer_hall_fri.h" 
if exist ".\TLSim\observer_hall_sf.c" del ".\TLSim\observer_hall_sf.c" 
if exist ".\TLSim\observer_hall_pcf.c" del ".\TLSim\observer_hall_pcf.c" 
if exist ".\TLSim\observer_hall_frm.h" del ".\TLSim\observer_hall_frm.h" 
if exist ".\TLSim\globals_observer_hall.*" del ".\TLSim\globals_observer_hall.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_hall\Metadata\observer_hall_SubsystemObject.dd" del ".\TLProj\observer_hall\Metadata\observer_hall_SubsystemObject.dd" 
if exist ".\TLProj\observer_hall" rmdir /s /q ".\TLProj\observer_hall" 
if exist ".\TLSim\StubCode\observer_hall" rmdir /s /q ".\TLSim\StubCode\observer_hall" 
if exist ".\TLProj\observer_hall\CGReport" rmdir /s /q ".\TLProj\observer_hall\CGReport" 
if exist ".\TLProj\observer_hall\Metadata" call:delete_if_empty ".\TLProj\observer_hall\Metadata"
if exist ".\TLProj\observer_hall" call:delete_if_empty ".\TLProj\observer_hall"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bemf
rem *********************************************************************************************


:delete_observer_bemf_files
set tl_subsys=observer_bemf
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bemf_fri.c" del ".\TLSim\observer_bemf_fri.c" 
if exist ".\TLSim\observer_bemf_fri.h" del ".\TLSim\observer_bemf_fri.h" 
if exist ".\TLSim\observer_bemf_sf.c" del ".\TLSim\observer_bemf_sf.c" 
if exist ".\TLSim\observer_bemf_pcf.c" del ".\TLSim\observer_bemf_pcf.c" 
if exist ".\TLSim\observer_bemf_frm.h" del ".\TLSim\observer_bemf_frm.h" 
if exist ".\TLSim\globals_observer_bemf.*" del ".\TLSim\globals_observer_bemf.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bemf\Metadata\observer_bemf_SubsystemObject.dd" del ".\TLProj\observer_bemf\Metadata\observer_bemf_SubsystemObject.dd" 
if exist ".\TLProj\observer_bemf" rmdir /s /q ".\TLProj\observer_bemf" 
if exist ".\TLSim\StubCode\observer_bemf" rmdir /s /q ".\TLSim\StubCode\observer_bemf" 
if exist ".\TLProj\observer_bemf\CGReport" rmdir /s /q ".\TLProj\observer_bemf\CGReport" 
if exist ".\TLProj\observer_bemf\Metadata" call:delete_if_empty ".\TLProj\observer_bemf\Metadata"
if exist ".\TLProj\observer_bemf" call:delete_if_empty ".\TLProj\observer_bemf"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit motoring
rem *********************************************************************************************


:delete_motoring_files
set tl_subsys=motoring
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\motoring_fri.c" del ".\TLSim\motoring_fri.c" 
if exist ".\TLSim\motoring_fri.h" del ".\TLSim\motoring_fri.h" 
if exist ".\TLSim\motoring_sf.c" del ".\TLSim\motoring_sf.c" 
if exist ".\TLSim\motoring_pcf.c" del ".\TLSim\motoring_pcf.c" 
if exist ".\TLSim\motoring_frm.h" del ".\TLSim\motoring_frm.h" 
if exist ".\TLSim\globals_motoring.*" del ".\TLSim\globals_motoring.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\motoring\Metadata\motoring_SubsystemObject.dd" del ".\TLProj\motoring\Metadata\motoring_SubsystemObject.dd" 
if exist ".\TLProj\motoring" rmdir /s /q ".\TLProj\motoring" 
if exist ".\TLSim\StubCode\motoring" rmdir /s /q ".\TLSim\StubCode\motoring" 
if exist ".\TLProj\motoring\CGReport" rmdir /s /q ".\TLProj\motoring\CGReport" 
if exist ".\TLProj\motoring\Metadata" call:delete_if_empty ".\TLProj\motoring\Metadata"
if exist ".\TLProj\motoring" call:delete_if_empty ".\TLProj\motoring"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bemf_6step
rem *********************************************************************************************


:delete_observer_bemf_6step_files
set tl_subsys=observer_bemf_6step
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bemf_6Step_fri.c" del ".\TLSim\observer_bemf_6Step_fri.c" 
if exist ".\TLSim\observer_bemf_6Step_fri.h" del ".\TLSim\observer_bemf_6Step_fri.h" 
if exist ".\TLSim\observer_bemf_6step_sf.c" del ".\TLSim\observer_bemf_6step_sf.c" 
if exist ".\TLSim\observer_bemf_6step_pcf.c" del ".\TLSim\observer_bemf_6step_pcf.c" 
if exist ".\TLSim\observer_bemf_6step_frm.h" del ".\TLSim\observer_bemf_6step_frm.h" 
if exist ".\TLSim\globals_observer_bemf_6step.*" del ".\TLSim\globals_observer_bemf_6step.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bemf_6Step\Metadata\observer_bemf_6step_SubsystemObject.dd" del ".\TLProj\observer_bemf_6Step\Metadata\observer_bemf_6step_SubsystemObject.dd" 
if exist ".\TLProj\observer_bemf_6Step" rmdir /s /q ".\TLProj\observer_bemf_6Step" 
if exist ".\TLSim\StubCode\observer_bemf_6Step" rmdir /s /q ".\TLSim\StubCode\observer_bemf_6Step" 
if exist ".\TLProj\observer_bemf_6Step\CGReport" rmdir /s /q ".\TLProj\observer_bemf_6Step\CGReport" 
if exist ".\TLProj\observer_bemf_6Step\Metadata" call:delete_if_empty ".\TLProj\observer_bemf_6Step\Metadata"
if exist ".\TLProj\observer_bemf_6Step" call:delete_if_empty ".\TLProj\observer_bemf_6Step"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin_lib
rem *********************************************************************************************


:delete_observer_bhspin_lib_files
set tl_subsys=observer_bhspin_lib
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin_lib_fri.c" del ".\TLSim\observer_bhspin_lib_fri.c" 
if exist ".\TLSim\observer_bhspin_lib_fri.h" del ".\TLSim\observer_bhspin_lib_fri.h" 
if exist ".\TLSim\observer_bhspin_lib_sf.c" del ".\TLSim\observer_bhspin_lib_sf.c" 
if exist ".\TLSim\observer_bhspin_lib_pcf.c" del ".\TLSim\observer_bhspin_lib_pcf.c" 
if exist ".\TLSim\observer_bhspin_lib_frm.h" del ".\TLSim\observer_bhspin_lib_frm.h" 
if exist ".\TLSim\globals_observer_bhspin_lib.*" del ".\TLSim\globals_observer_bhspin_lib.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin_lib\Metadata\observer_bhspin_lib_SubsystemObject.dd" del ".\TLProj\observer_bhspin_lib\Metadata\observer_bhspin_lib_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin_lib" rmdir /s /q ".\TLProj\observer_bhspin_lib" 
if exist ".\TLSim\StubCode\observer_bhspin_lib" rmdir /s /q ".\TLSim\StubCode\observer_bhspin_lib" 
if exist ".\TLProj\observer_bhspin_lib\CGReport" rmdir /s /q ".\TLProj\observer_bhspin_lib\CGReport" 
if exist ".\TLProj\observer_bhspin_lib\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin_lib\Metadata"
if exist ".\TLProj\observer_bhspin_lib" call:delete_if_empty ".\TLProj\observer_bhspin_lib"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit mil
rem *********************************************************************************************


:delete_mil_files
set tl_subsys=mil
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\MIL_fri.c" del ".\TLSim\MIL_fri.c" 
if exist ".\TLSim\MIL_fri.h" del ".\TLSim\MIL_fri.h" 
if exist ".\TLSim\mil_sf.c" del ".\TLSim\mil_sf.c" 
if exist ".\TLSim\mil_pcf.c" del ".\TLSim\mil_pcf.c" 
if exist ".\TLSim\mil_frm.h" del ".\TLSim\mil_frm.h" 
if exist ".\TLSim\globals_mil.*" del ".\TLSim\globals_mil.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\MIL\Metadata\mil_SubsystemObject.dd" del ".\TLProj\MIL\Metadata\mil_SubsystemObject.dd" 
if exist ".\TLProj\MIL" rmdir /s /q ".\TLProj\MIL" 
if exist ".\TLSim\StubCode\MIL" rmdir /s /q ".\TLSim\StubCode\MIL" 
if exist ".\TLProj\MIL\CGReport" rmdir /s /q ".\TLProj\MIL\CGReport" 
if exist ".\TLProj\MIL\Metadata" call:delete_if_empty ".\TLProj\MIL\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\MIL" call:delete_if_empty ".\TLProj\MIL"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit pid
rem *********************************************************************************************


:delete_pid_files
set tl_subsys=pid
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\PID_fri.c" del ".\TLSim\PID_fri.c" 
if exist ".\TLSim\PID_fri.h" del ".\TLSim\PID_fri.h" 
if exist ".\TLSim\pid_sf.c" del ".\TLSim\pid_sf.c" 
if exist ".\TLSim\pid_pcf.c" del ".\TLSim\pid_pcf.c" 
if exist ".\TLSim\pid_frm.h" del ".\TLSim\pid_frm.h" 
if exist ".\TLSim\globals_pid.*" del ".\TLSim\globals_pid.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\PID\Metadata\pid_SubsystemObject.dd" del ".\TLProj\PID\Metadata\pid_SubsystemObject.dd" 
if exist ".\TLProj\PID" rmdir /s /q ".\TLProj\PID" 
if exist ".\TLSim\StubCode\PID" rmdir /s /q ".\TLSim\StubCode\PID" 
if exist ".\TLProj\PID\CGReport" rmdir /s /q ".\TLProj\PID\CGReport" 
if exist ".\TLProj\PID\Metadata" call:delete_if_empty ".\TLProj\PID\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\PID" call:delete_if_empty ".\TLProj\PID"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit gain_scheduling
rem *********************************************************************************************


:delete_gain_scheduling_files
set tl_subsys=gain_scheduling
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\Gain_Scheduling_fri.c" del ".\TLSim\Gain_Scheduling_fri.c" 
if exist ".\TLSim\Gain_Scheduling_fri.h" del ".\TLSim\Gain_Scheduling_fri.h" 
if exist ".\TLSim\gain_scheduling_sf.c" del ".\TLSim\gain_scheduling_sf.c" 
if exist ".\TLSim\gain_scheduling_pcf.c" del ".\TLSim\gain_scheduling_pcf.c" 
if exist ".\TLSim\gain_scheduling_frm.h" del ".\TLSim\gain_scheduling_frm.h" 
if exist ".\TLSim\globals_gain_scheduling.*" del ".\TLSim\globals_gain_scheduling.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\Gain_Scheduling\Metadata\gain_scheduling_SubsystemObject.dd" del ".\TLProj\Gain_Scheduling\Metadata\gain_scheduling_SubsystemObject.dd" 
if exist ".\TLProj\Gain_Scheduling" rmdir /s /q ".\TLProj\Gain_Scheduling" 
if exist ".\TLSim\StubCode\Gain_Scheduling" rmdir /s /q ".\TLSim\StubCode\Gain_Scheduling" 
if exist ".\TLProj\Gain_Scheduling\CGReport" rmdir /s /q ".\TLProj\Gain_Scheduling\CGReport" 
if exist ".\TLProj\Gain_Scheduling\Metadata" call:delete_if_empty ".\TLProj\Gain_Scheduling\Metadata"
if exist ".\TLProj\Gain_Scheduling" call:delete_if_empty ".\TLProj\Gain_Scheduling"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit controller
rem *********************************************************************************************


:delete_controller_files
set tl_subsys=controller
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\Controller_fri.c" del ".\TLSim\Controller_fri.c" 
if exist ".\TLSim\Controller_fri.h" del ".\TLSim\Controller_fri.h" 
if exist ".\TLSim\controller_sf.c" del ".\TLSim\controller_sf.c" 
if exist ".\TLSim\controller_pcf.c" del ".\TLSim\controller_pcf.c" 
if exist ".\TLSim\controller_frm.h" del ".\TLSim\controller_frm.h" 
if exist ".\TLSim\globals_controller.*" del ".\TLSim\globals_controller.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\Controller\Metadata\controller_SubsystemObject.dd" del ".\TLProj\Controller\Metadata\controller_SubsystemObject.dd" 
if exist ".\TLProj\Controller" rmdir /s /q ".\TLProj\Controller" 
if exist ".\TLSim\StubCode\Controller" rmdir /s /q ".\TLSim\StubCode\Controller" 
if exist ".\TLProj\Controller\CGReport" rmdir /s /q ".\TLProj\Controller\CGReport" 
if exist ".\TLProj\Controller\Metadata" call:delete_if_empty ".\TLProj\Controller\Metadata"
if exist ".\TLProj\Controller" call:delete_if_empty ".\TLProj\Controller"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)

:delete_if_empty
set _TMP=
for /f "delims=" %%a in ('dir /b %1') do set _TMP="%%a"
IF {%_TMP%}=={} (
      set _empty=Empty
      if exist %1 rmdir /s /q %1
)
