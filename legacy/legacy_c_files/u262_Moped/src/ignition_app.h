/****************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: ignition_app.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 12.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are specific to the ignition application.
*
* Change history
* {Date}: {Developer}: {Changes}
****************************************************************************************************************/
#ifndef IGNITION_APP_H
#define IGNITION_APP_H

#include "S32K144.h"
#include "simple_types.h"
#include "userdefine.h"

extern const uint16_t CAL_FullLoadCountsCurve[];

typedef struct
{
  uint16_t EngineSpeed_rpm;
  bool bExcessCurrentDetected;
  bool bEngineOn;
  bool bEnabled;
} IGNITION_t;

void Ignition_MainLoopHandler(uint16_t *, uint8_t *, int16_t *);
void Ignition_TimerA_CompareISR(void);
void Ignition_TimerD_CaptureISR(void);
void Ignition_TimerB_CompareISR(void);
void Ignition_TimerC_OverflowISR(void);

void Ignition_ZeroPulseIsr(uint16_t tZeroPulse );

void Ignition_AdvPulseIsr(uint16_t tAdvPulse );
bool Ignition_DeaccelerationStatus(void);

void Ignition_Init(void);
void Ignition_Enable(void);
void Ignition_Disable(void);
void Ignition_HardDisable(void);
void Ignition_Reset(void);

void Ignition_ClearExcessCurrentFault(void);
void Ignition_RaiseExcessCurrentFault(void);

bool Ignition_DeaccelerationStatus(void);


#endif
