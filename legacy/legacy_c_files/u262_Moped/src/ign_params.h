#ifndef IGN_PARAMS_H
#define IGN_PARAMS_H

#include "S32K144.h"
#include "simple_types.h"
#include "userdefine.h"

extern volatile uint8_t MEAS_PA_ACC_FLAG;
extern volatile uint8_t MEAS_PA_DEACC_FLAG;
extern volatile uint16_t MEAS_CPS_RPM;
extern volatile uint16_t MEAS_ENGINE_LOAD;
extern volatile int16_t MEAS_IGN_TIMING;
extern volatile uint8_t MEAS_PIP_WIDTH;
extern const uint16_t CAL_FullLoadCountsCurve[];

extern   uint16_t CAL_SPARK_CUTOFF_RPM;
extern   uint16_t CAL_ANALOG_FIRING_RPM;
extern    uint8_t CAL_COUNT_IGN_HW_TRIGGER;
extern    uint8_t CAL_IGN_MAP_LOAD_STEP;
extern    uint16_t CAL_LOAD_DETECTION_MAX_RPM;
extern    uint16_t CAL_DEACC_LOWER_RPM;
extern   uint16_t CAL_SPARK_CUTOFF_RPM;
extern   uint16_t CAL_EXTENDED_DWELL_COUNTS;
extern    uint16_t CAL_IDLE_STABILITY_MAX_RPM;

extern   uint8_t CAL_EXTENDED_DWELL_ENABLE;
extern   uint16_t CAL_RPM_EXTENDED_DWELL;
extern   uint16_t CAL_EXTEND_DWELL_STRIP_COUNTS;
extern   uint8_t CAL_DEACCELARTION_DIGITAL_FIRING;
extern   int8_t CAL_IgnTimingMap[46][11];
extern   uint8_t CAL_ENABLE_ACC_CURVE;
extern   uint8_t CAL_ENABLE_DEACC_MAP;
extern   uint8_t CAL_NO_CYCLES_ACC;
extern   uint8_t CAL_NO_CYCLES_DEACC;
extern   uint8_t CAL_NO_CYCLES_RPM_INCREASING;
extern   uint8_t CAL_NO_CYCLES_RPM_DECREASING;
extern   uint8_t CAL_NO_CYCLES_RPM_STABLE;
extern   int16_t CAL_AccelerationCurve[];
extern   int16_t CAL_DeaccelerationCurve[];
extern   uint16_t CAL_DWELL_TIME;
extern   uint16_t CAL_ENGINE_STABLE_RPM;
extern   int8_t CAL_CorrectiveAngleCurve[46];
extern   uint16_t CAL_CORRECTIVE_MAP_RPM_MAX;
extern   uint16_t CAL_DEACC_STABLE_RPM;
extern   uint8_t CAL_CORRECTIVE_CURVE_TIME_SEC;
extern   uint8_t CAL_N_ENGINE_REV_UP;
extern   uint8_t CAL_N_ENGINE_REV_DWN;
extern   uint8_t CAL_N_ENGINE_REV;
extern   uint16_t CAL_DEACC_N_SEC_ENABLE;

extern   uint16_t CAL_PIP_WIDTH_DEG;
extern   uint16_t CAL_PIP_BASE_DEG;
extern   uint16_t CAL_RPM_HW_TRG_MIN  ;
extern   uint16_t CAL_RPM_HW_TRG_MAX  ;
extern   uint16_t CAL_ACC_DELTA_RPM_LIMIT;
extern   uint16_t CAL_DEACC_DELTA_RPM_LIMIT;
extern   uint16_t CAL_DEACC_RPM_MAX;
extern   uint16_t CAL_LOAD_FILTER_COEFF;
extern   uint8_t CAL_N_CYCLES_FAST_WOT;
extern   uint8_t CAL_MAX_LOAD_FAST_WOT;

extern   uint16_t CAL_SEC_DWELL_TIME;
extern   uint16_t CAL_PRI_TO_SEC_SPARK_TIME_USEC;
extern   uint8_t CAL_SEC_SPARK_ENABLE;

extern   uint16_t CAL_PA_ACC_DELTA_RPM_LIMIT;
extern   uint16_t CAL_PA_DEACC_DELTA_RPM_LIMIT;
extern   uint8_t CAL_PA_ENABLE_ACC;
extern   uint8_t CAL_PA_ENABLE_DEACC;
extern   uint8_t CAL_PA_NO_CYCLES_ACC;
extern   uint8_t CAL_PA_NO_CYCLES_DEACC;
extern   uint8_t CAL_PA_NO_CYCLES_RPM_INCREASING;
extern   uint8_t CAL_PA_NO_CYCLES_RPM_DECREASING;
extern   uint8_t CAL_PA_NO_CYCLES_RPM_STABLE;
extern   uint16_t CAL_PA_DEACC_LOWER_RPM ;
extern   uint16_t CAL_PA_DEACC_RPM_MAX ;
extern   uint16_t CAL_PA_DEACC_N_SEC_ENABLE ;
extern   uint16_t CAL_PA_DEACC_STABLE_RPM ;










#endif /* IGN_PARAMS_H */
