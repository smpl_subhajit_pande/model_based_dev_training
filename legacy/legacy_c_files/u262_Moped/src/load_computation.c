/**************************************************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: load_computation.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 11.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are related to engine load computation.
* Change history
* +---------+------------+---------------------------------------------+-----------+
* | Version |    Date    |                Changelog                    | Author    |
* +---------+------------+---------------------------------------------+-----------+
* | 2.00    | 19 Dec 16  | StrokeDetctionAlgo1, stroke detctionAlgo2,  | Sandeep J |
* |         |            | ComputeRawLoadAlgo1 and ComputeRawLoadAlgo2 |           |
* |         |            | functions added.                            |           |
* +---------+------------+---------------------------------------------+-----------+
***************************************************************************************************************************************************/
#include "load_computation.h"

#define FILT_FACTOR 5
#define PHASE_AGREEMENT_CYCLES 20
#define PHASE_DISAGREEMENT_CYCLES 10

static uint8_t nPhaseAgreement = 0, nPhaseDisagreement = 0;
static volatile uint16_t LoadValFilt1Factored = 0, LoadValFilt2Factored = 0;
bool bPrevTdcCompression = false, bStrokeDetected = false;

/*********************************************************************************************************************************************
function          : StrokeDetectionAlgo1
description       : This function detects the engine stroke using current zero to zero and previous zero
                    to zero time reference.
Input parameters  : current zero to zero and previous zero to zero time reference.
Output parameters : None
Return value      : None
***********************************************************************************************************************************************/
void StrokeDetectionAlgo1(uint32_t DeltaZeroToZero, uint32_t PrevDeltaZeroToZero)
{
    if(PrevDeltaZeroToZero > DeltaZeroToZero)
    {
        if(bPrevTdcCompression)
        {
            nPhaseAgreement++;
            nPhaseDisagreement = 0;
        }
        else
        {
            nPhaseAgreement = 0;
            nPhaseDisagreement++;
        }

        if(nPhaseAgreement >= PHASE_AGREEMENT_CYCLES)
        {
            nPhaseAgreement = PHASE_AGREEMENT_CYCLES;
            bStrokeDetected = true;
        }
        else if(nPhaseDisagreement >= PHASE_DISAGREEMENT_CYCLES)
        {
            nPhaseDisagreement = PHASE_DISAGREEMENT_CYCLES;
            bStrokeDetected = false;
        }

        if(!bStrokeDetected)
        {
            bPrevTdcCompression = false;
        }
        else
        {
            bPrevTdcCompression = !bPrevTdcCompression;
        }
    }
    else
    {
        if(!bPrevTdcCompression)
        {
            nPhaseAgreement++;
            nPhaseDisagreement = 0;
        }
        else
        {
            nPhaseAgreement = 0;
            nPhaseDisagreement++;
        }
        if(nPhaseAgreement >= PHASE_AGREEMENT_CYCLES)
        {
            nPhaseAgreement = PHASE_AGREEMENT_CYCLES;
            bStrokeDetected = true;
        }
        else if(nPhaseDisagreement >= PHASE_DISAGREEMENT_CYCLES)
        {
            nPhaseDisagreement = PHASE_DISAGREEMENT_CYCLES;
            bStrokeDetected = false;
        }

        if(!bStrokeDetected)
        {
            bPrevTdcCompression = true;
        }
        else
        {
            bPrevTdcCompression = !bPrevTdcCompression;
        }
    }
}


/*********************************************************************************************************************************************
function          : StrokeDetectionAlgo2
description       : This function detects the current stroke using current zero to zero
                    and previous zero to zero.Stroke is detected only when difference
                    between current zero to zero and previous zero to zero is greater
                    than noise figure(offset).
Input parameters  : current zero to zero, previous zero to zero time reference and offset
                    between zero to zero and previous zero to zero .
Output parameters : None
Return value      : None
***********************************************************************************************************************************************/
void StrokeDetectionAlgo2(uint32_t DeltaZeroToZero, uint32_t PrevDeltaZeroToZero, uint8_t offset)
{
    if(PrevDeltaZeroToZero > DeltaZeroToZero)
    {
        if(PrevDeltaZeroToZero >= (DeltaZeroToZero + offset))
        {
            if(bPrevTdcCompression)
            {
                nPhaseAgreement++;
                nPhaseDisagreement = 0;
            }
            else
            {
                nPhaseAgreement = 0;
                nPhaseDisagreement++;
            }

            if(nPhaseAgreement >= PHASE_AGREEMENT_CYCLES)
            {
                nPhaseAgreement = PHASE_AGREEMENT_CYCLES;
                bStrokeDetected = true;
            }
            else if(nPhaseDisagreement >= PHASE_DISAGREEMENT_CYCLES)
            {
                nPhaseDisagreement = PHASE_DISAGREEMENT_CYCLES;
                bStrokeDetected = false;
            }
        }
        else
        {
            nPhaseAgreement = 0 ;
            nPhaseDisagreement = 0;
        }

        if(!bStrokeDetected)
        {
            bPrevTdcCompression = false;
        }
        else
        {
            bPrevTdcCompression = !bPrevTdcCompression;
        }
    }
    else
    {
        if(DeltaZeroToZero >= (PrevDeltaZeroToZero + offset))
        {
            if(!bPrevTdcCompression)
            {
                nPhaseAgreement++;
                nPhaseDisagreement = 0;
            }
            else
            {
                nPhaseAgreement = 0;
                nPhaseDisagreement++;
            }
            if(nPhaseAgreement >= PHASE_AGREEMENT_CYCLES)
            {
                nPhaseAgreement = PHASE_AGREEMENT_CYCLES;
                bStrokeDetected = true;
            }
            else if(nPhaseDisagreement >= PHASE_DISAGREEMENT_CYCLES)
            {
                nPhaseDisagreement = PHASE_DISAGREEMENT_CYCLES;
                bStrokeDetected = false;
            }
        }
        else
        {
            nPhaseAgreement = 0;
            nPhaseDisagreement = 0;
        }
        if(!bStrokeDetected)
        {
            bPrevTdcCompression = true;
        }
        else
        {
            bPrevTdcCompression = !bPrevTdcCompression;
        }
    }
}


/*********************************************************************************************************************************************
function          : ComputeRawLoadAlgo1
description       : This function computes the raw load when correct stroke is detected.
Input parameters  : current zero to zero and previous zero to zero time reference.
Output parameters : None
Return value      : Raw load value.
***********************************************************************************************************************************************/
uint32_t ComputeRawLoadAlgo1(uint32_t DeltaZeroToZero, uint32_t PrevDeltaZeroToZero)
{
    static uint32_t RawLoad, tPrvToPrvDiffZeroToZero;
    uint32_t tAvgDiffZeroToZero = 0;

    tAvgDiffZeroToZero = (DeltaZeroToZero + tPrvToPrvDiffZeroToZero) >> 1;

    if(!bStrokeDetected)
    {
        RawLoad = 0;
    }
    else
    {
        if(bPrevTdcCompression)
        {
            if(tAvgDiffZeroToZero > PrevDeltaZeroToZero)
            {
                RawLoad = tAvgDiffZeroToZero - PrevDeltaZeroToZero;
            }
            else
            {
                RawLoad = 0;
            }
        }
        else
        {
            if(PrevDeltaZeroToZero > tAvgDiffZeroToZero)
            {
                RawLoad = PrevDeltaZeroToZero - tAvgDiffZeroToZero;
            }
            else
            {
                RawLoad = 0;
            }
        }
        tPrvToPrvDiffZeroToZero = PrevDeltaZeroToZero;
    }

    return RawLoad;
}


/*********************************************************************************************************************************************
function          : ComputeRawLoadAlgo2
description       : This function computes the raw load starting from first CPS cycle.
Input parameters  : current zero to zero and previous zero to zero time reference.
Output parameters : None
Return value      : Raw load value.
***********************************************************************************************************************************************/
uint32_t ComputeRawLoadAlgo2(uint32_t DeltaZeroToZero, uint32_t PrevDeltaZeroToZero)
{
    static uint32_t RawLoad, tPrvToPrvDiffZeroToZero;
    uint32_t tAvgDiffZeroToZero = 0;

    tAvgDiffZeroToZero = (DeltaZeroToZero + tPrvToPrvDiffZeroToZero) >> 1;

    if(bPrevTdcCompression)
    {
        if(tAvgDiffZeroToZero > PrevDeltaZeroToZero)
        {
            RawLoad = tAvgDiffZeroToZero - PrevDeltaZeroToZero;
        }
        else
        {
            RawLoad = 0;
        }
    }
    else
    {
        if(PrevDeltaZeroToZero > tAvgDiffZeroToZero)
        {
            RawLoad = PrevDeltaZeroToZero - tAvgDiffZeroToZero;
        }
        else
        {
            RawLoad = 0;
        }
    }
    tPrvToPrvDiffZeroToZero = PrevDeltaZeroToZero;
    return RawLoad;
}


/*****************************************************************************************
function          : prvNormalizeLoad
description       : This function computes the raw percentage load on vehicle with
                    reference to full load counts.
Input parameters  : full load and computed raw load
Output parameters : None
Return value      : 'LoadValueRaw_percent'- percentage raw load
*****************************************************************************************/
uint16_t NormalizeRawLoad(uint32_t FullLoadValue, uint32_t ComputedRawLoad)
{
    uint16_t LoadValueRaw_percent;

    if(ComputedRawLoad > FullLoadValue)
    {
        LoadValueRaw_percent = 100;
    }
    else
    {
        LoadValueRaw_percent = (uint16_t)(((uint32_t)ComputedRawLoad * 100) / (FullLoadValue));
    }

    return LoadValueRaw_percent;
}


/*****************************************************************************************
function          : ResetLoadVariables
description       : This function resets all load detection parameters
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void ResetLoadVariables(void)
{
    nPhaseAgreement = 0;
    nPhaseDisagreement = 0;
    bStrokeDetected = false;
    return;
}


/*****************************************************************************************
function          : SaturateFiltDuringFastWOT
description       : This function bypasses load filter if sudden change in load is detected.
Input parameters  : percentage raw load and filered load
Output parameters : None
Return value      : None
*****************************************************************************************/
void SaturateFiltDuringFastWOT(uint16_t RawLoadValue_percent, uint16_t FilteredLoadValue_percentage, uint8_t FastWotDeltaLoad, uint8_t FastWotMaxLoad, uint8_t nCyclesFastWot)
{
    static uint8_t nLoadStepChange = 0;

    if(RawLoadValue_percent > FilteredLoadValue_percentage)
    {
        if((RawLoadValue_percent - FilteredLoadValue_percentage) > FastWotDeltaLoad)
        {
            nLoadStepChange++;
            if(nLoadStepChange >= nCyclesFastWot)
            {
                nLoadStepChange = 0;
                LoadValFilt1Factored = ((uint16_t)FastWotMaxLoad * FILT_FACTOR);
                LoadValFilt2Factored = ((uint16_t)FastWotMaxLoad * FILT_FACTOR);
            }
        }
        else
        {
            nLoadStepChange = 0;
        }
    }
    else
    {
        nLoadStepChange = 0;
    }
}


/*****************************************************************************************
function          : LoadDigitalFilter
description       : This function is a two stage digital filter.
Input parameters  : 'LoadRawVal' - Raw value of load normalised to 100.
Output parameters : None
Return value      : 'SecondFiltVal' - Second stage filtered value of load.
*****************************************************************************************/
uint16_t LoadDigitalFilter(uint16_t LoadRawVal, uint16_t LoadFilterCoeff)
{
    uint16_t FirstFiltVal, SecondFiltVal;

    LoadValFilt1Factored = ((LoadFilterCoeff * LoadValFilt1Factored) + (uint32_t)(((100 * FILT_FACTOR) - (FILT_FACTOR * LoadFilterCoeff)) * LoadRawVal) + 50) / 100;
    FirstFiltVal = LoadValFilt1Factored / FILT_FACTOR;

    LoadValFilt2Factored = ((LoadFilterCoeff * LoadValFilt2Factored) + (uint32_t)(((100 * FILT_FACTOR) - (FILT_FACTOR * LoadFilterCoeff)) * FirstFiltVal) + 50) / 100;
    SecondFiltVal = LoadValFilt2Factored / FILT_FACTOR;

    return SecondFiltVal;
}

