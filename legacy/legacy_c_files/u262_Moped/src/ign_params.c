
#include "ign_params.h"

/**************************Meaasurement Parameters****************************
  Always Add Measurement parameters in given space.
  Do not add Measurement parameters in Calibration parameter space i.e. after
  #pragma section @@INIT @CALIB_DATA AT 0FF400H
******************************************************************************/

volatile uint16_t MEAS_CPS_RPM = 0;

volatile uint16_t MEAS_ENGINE_LOAD = 0;

volatile int16_t MEAS_IGN_TIMING = 0;

volatile uint8_t MEAS_PIP_WIDTH = 0;

volatile uint8_t MEAS_PA_ACC_FLAG = 0;

volatile uint8_t MEAS_PA_DEACC_FLAG =0;


/************************** Calibration Parameters****************************
  Always Add Calibration parameters in given space. Also try to maintain
  sequence.
  NOTE: TO REMOVE VARIABLE FROM CCP, REMOVE ITS INFORMATION NOTED ABOVE THE
        PARAMETER DECLARATION AND REPLACE "SED_CALIB_VARS CCP_CONST" WITH "SED_CALIB_VARS CCP_CONST".
******************************************************************************/



uint16_t CAL_PIP_BASE_DEG                    = 5;

uint16_t CAL_PIP_WIDTH_DEG                   = 49;

uint16_t CAL_RPM_HW_TRG_MIN                  = 800;

uint16_t CAL_RPM_HW_TRG_MAX                 = 1800;

uint16_t CAL_ACC_DELTA_RPM_LIMIT             = 25;

uint16_t CAL_DEACC_DELTA_RPM_LIMIT           = 70;

uint16_t CAL_DEACC_RPM_MAX                   = 1350;

uint8_t CAL_ENABLE_ACC_CURVE                 = 1;

uint8_t CAL_ENABLE_DEACC_MAP                 = 1;

uint8_t CAL_NO_CYCLES_ACC                    = 3;

uint8_t CAL_NO_CYCLES_DEACC                  = 3;

uint8_t CAL_NO_CYCLES_RPM_INCREASING         = 3;

uint8_t CAL_NO_CYCLES_RPM_DECREASING         = 5;

uint8_t CAL_NO_CYCLES_RPM_STABLE          = 10;

uint8_t CAL_N_ENGINE_REV_UP               = 0;

uint8_t CAL_N_ENGINE_REV_DWN              = 3;

uint8_t CAL_N_ENGINE_REV                = 4;

uint8_t CAL_DEACCELARTION_DIGITAL_FIRING     = 1;

int16_t CAL_AccelerationCurve[] = {
    15,  // 1000
    15,  // 1200
    15,  // 1400
    15,  // 1600
    15,  // 1800
    15,  // 2000
    15,  // 2200
    15,  // 2400
    15,  // 2600
    15,  // 2800
    15,  // 3000
    15,  // 3200
    15,  // 3400
    15,  // 3600
    15,  // 3800
    15,  // 4000
    15,  // 4200
    15,  // 4400
    15,  // 4600
    15,  // 4800
    15,  // 5000
    15,  // 5200
    15,  // 5400
    15,  // 5600
    15,  // 5800
    15,  // 6000
    15,  // 6200
    15,  // 6400
    15,  // 6600
    15,  // 6800
    15,  // 7000
    15,  // 7200
    15,  // 7400
    15,  // 7600
    15,  // 7800
    15,  // 8000
    15,  // 8200
    15,  // 8400
    15,  // 8600
    15,  // 8800
    15,  // 9000
    15,  // 9200
    15,  // 9400
    15,  // 9600
    15,  // 9800
    15   // 10000
};


int16_t CAL_DeaccelerationCurve[] = {
    20,  // 600
    20,  // 650
    20,  // 700
    20,  // 750
    20,  // 800
    20,  // 850
    20,  // 900
    20,  // 950
    20,  // 1000
    20,  // 1050
    20,  // 1100
    20,  // 1150
    20,  // 1200
    20,  // 1250
    20,  // 1300
    20,  // 1350
    20,  // 1400
    20,  // 1450
    20,  // 1500
    20,  // 1550
    20,  // 1600
    20,  // 1650
    20,  // 1700
    20,  // 1750
    20,  // 1800
    20,  // 1850
    20,  // 1900
    20,  // 1950
    20,  // 2000
    20,  // 2050
    20,  // 2100
    20,  // 2150
    20,  // 2200
    20,  // 2250
    20,  // 2300
    20,  // 2350
    20,  // 2400
    20,  // 2450
    20,  // 2500
    20,  // 2550
    20,  // 2600
    20,  // 2650
    20,  // 2700
    20,  // 2750
    20,  // 2800
    20,  // 2850
    20,  // 2900
    20,  // 2950
    20,  // 3000
    20,  // 3050
    20,  // 3100
    20,  // 3150
    20,  // 3200
    20,  // 3250
    20,  // 3300
    20,  // 3350
    20,  // 3400
    20,  // 3450
    20,  // 3500
    20,  // 3550
    20,  // 3600
    20,  // 3650
    20,  // 3700
    20,  // 3750
    20,  // 3800
    20,  // 3850
    20,  // 3900
    20,  // 3950
    20   // 4000
};

uint16_t CAL_DEACC_N_SEC_ENABLE              = 10;

uint16_t CAL_DWELL_TIME                      = 2800;

uint8_t CAL_EXTENDED_DWELL_ENABLE            = 1;

uint16_t CAL_RPM_EXTENDED_DWELL              = 800;

uint16_t CAL_EXTEND_DWELL_STRIP_COUNTS       = 65000;

uint16_t CAL_EXTENDED_DWELL_COUNTS           = 65000;

uint16_t CAL_SPARK_CUTOFF_RPM                = 7000;

uint16_t CAL_IDLE_STABILITY_MAX_RPM          = 2000;

uint8_t  CAL_COUNT_IGN_HW_TRIGGER            = 0;

uint16_t CAL_LOAD_FILTER_COEFF               = 85;

uint8_t CAL_MAX_LOAD_FAST_WOT                = 90;

uint8_t CAL_IGN_MAP_LOAD_STEP                = 10;

uint16_t CAL_LOAD_DETECTION_MAX_RPM          = 8000;

uint16_t CAL_DEACC_LOWER_RPM                 = 675;

uint16_t CAL_DEACC_STABLE_RPM                 = 1000;

uint8_t CAL_SEC_SPARK_ENABLE = 1;  /* Enable or disable secondary spark till corrective map logic */

uint16_t CAL_PRI_TO_SEC_SPARK_TIME_USEC = 352; /* Primary to Secondary dwell start time in micro seconds */

uint16_t CAL_SEC_DWELL_TIME = 2000; /* Secondary dwell on time in microseconds. */

int8_t CAL_IgnTimingMap[46][11] = {
    {5,5,5,5,5,5,5,5,5,5,5},                //1000
    {5,5,5,5,5,5,5,5,5,5,5},                //1200
    {5,5,5,5,5,5,5,5,5,5,5},                //1400
    {5,5,5,5,5,5,5,5,5,5,5},                //1600
    {5,5,5,5,5,5,5,5,5,5,5},                //1800
    {10,10,10,10,15,15,15,15,12,12,12},     //2000
    {10,10,10,10,15,15,15,15,14,14,12},     //2200
    {15,15,15,15,21,21,17,17,14,14,12},     //2400
    {20,20,20,20,23,23,22,21,15,15,13},     //2600
    {25,25,25,25,25,23,22,21,16,16,13},     //2800
    {30,30,30,30,25,23,23,21,18,16,13},     //3000
    {34,34,34,31,26,24,24,22,19,16,13},     //3200
    {39,39,39,31,30,25,25,23,20,16,14},     //3400
    {43,43,39,31,30,26,26,24,20,16,14},     //3600
    {43,43,40,32,30,27,27,25,20,16,14},     //3800
    {43,43,40,33,33,33,28,26,20,16,14},     //4000
    {43,43,42,34,33,33,28,26,20,16,14},     //4200
    {45,45,45,36,33,33,28,27,22,16,14},     //4400
    {46,46,46,37,33,33,29,27,22,16,14},     //4600
    {46,46,46,38,33,33,30,28,22,16,14},     //4800
    {46,46,46,39,35,33,31,29,24,17,14},     //5000
    {46,46,46,40,37,37,34,30,24,19,17},     //5200
    {46,46,46,41,38,37,35,31,28,23,21},     //5400
    {46,46,46,41,40,40,40,38,30,23,21},     //5600
    {46,46,46,42,40,40,40,38,30,23,21},     //5800
    {46,46,46,43,40,40,40,38,33,25,21},     //6000
    {46,46,46,43,41,40,40,38,35,25,21},     //6200
    {46,46,46,44,42,39,36,36,35,25,21},     //6400
    {46,46,46,45,44,40,36,36,36,28,22},     //6600
    {46,46,46,45,44,41,37,36,36,30,22},     //6800
    {46,46,46,45,44,42,38,36,36,31,22},     //7000
    {44,44,44,44,44,42,39,36,36,32,22},     //7200
    {38,38,38,40,40,40,40,34,34,32,21},     //7400
    {32,32,32,33,33,33,33,32,30,28,20},     //7600
    {26,26,26,26,26,26,26,26,21,21,19},     //7800
    {15,15,15,15,15,15,15,15,15,15,15},     //8000
    {0,0,0,0,0,0,0,0,0,0,0},                //8200
    {0,0,0,0,0,0,0,0,0,0,0},                //8400
    {0,0,0,0,0,0,0,0,0,0,0},                //8600
    {0,0,0,0,0,0,0,0,0,0,0},                //8800
    {0,0,0,0,0,0,0,0,0,0,0},                //9000
    {0,0,0,0,0,0,0,0,0,0,0},                //9200
    {0,0,0,0,0,0,0,0,0,0,0},                //9400
    {0,0,0,0,0,0,0,0,0,0,0},                //9600
    {0,0,0,0,0,0,0,0,0,0,0},                //9800
    {0,0,0,0,0,0,0,0,0,0,0},                //10000
};

uint16_t CAL_CORRECTIVE_MAP_RPM_MAX = 6000;

uint8_t CAL_CORRECTIVE_CURVE_TIME_SEC = 45;  /* Time in seconds upto which corrective Map will be executed after engine on */

int8_t CAL_CorrectiveAngleCurve[46] = {5, 5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


uint16_t CAL_PA_ACC_DELTA_RPM_LIMIT          = 25;

uint16_t CAL_PA_DEACC_DELTA_RPM_LIMIT        = 70;

uint8_t CAL_PA_ENABLE_ACC                    = 1;

uint8_t CAL_PA_ENABLE_DEACC                 = 1;

uint8_t CAL_PA_NO_CYCLES_ACC                = 3;

uint8_t CAL_PA_NO_CYCLES_DEACC               = 3;

uint8_t CAL_PA_NO_CYCLES_RPM_INCREASING      = 3;

uint8_t CAL_PA_NO_CYCLES_RPM_DECREASING      = 5;

uint8_t CAL_PA_NO_CYCLES_RPM_STABLE       = 10;

uint16_t CAL_PA_DEACC_LOWER_RPM            = 675;

uint16_t CAL_PA_DEACC_RPM_MAX             = 1350;

uint16_t CAL_PA_DEACC_N_SEC_ENABLE        = 10;

uint16_t CAL_PA_DEACC_STABLE_RPM          = 1000;
