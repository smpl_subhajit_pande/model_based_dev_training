/**************************************************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: load_computation.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 11.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are common across all ignition variants.
* Change history
* +---------+------------+---------------------------------------------+-----------+
* | Version |    Date    |                Changelog                    | Author    |
* +---------+------------+---------------------------------------------+-----------+
* | 1.00    | 19 Dec 16  | ComputeEngienSpeed, CheckEngineCranked and  | Sandeep J |
* |         |            | CutOffIgnition functions updated for        |           |
* |         |            | function arguments.                         |           |
* +---------+------------+---------------------------------------------+-----------+
* | 2.00    | 28 Dec 16  | CutOffIgnition function updated for         | Sandeep J |
* |         |            | initial value of cut off spark flag         |           |
* +---------+------------+---------------------------------------------+-----------+
* | 2.01    | 02 Jan 17  | CutOffIgnition function updated for logic   | Sandeep J |
* |         |            | to resume from cutoff.                      |           |
* +---------+------------+---------------------------------------------+-----------+
***************************************************************************************************************************************************/
#include "ignition_app.h"
#include "ignition_common.h"
#include "ign_params.h"

#define RPM_CALC_FACTOR      60000000

static uint8_t nRpmCrossedCrankRpm = 0;

/**********************************************************************************************************************************************
function          : ComputeEngineSpeed
description       : This function is used to compute Engine Speed in RPM by taking
                    average of current zero to zero and previous zero to zero time.
Input parameters  : First crank pulse flag, timer resolution, zero to zero and previous
                    zero to zero time reference
Output parameters : flag FirstCrankPulse
Return value      : engine rpm
************************************************************************************************************************************************/
uint16_t ComputeEngineSpeed(uint32_t DiffZerotoZero, uint32_t PrevDiffZerotoZero, bool *bFirstCrankPulse, uint8_t IgnTimerResolution)
{
    uint32_t tDiffZeroToZeroAvg = 0;
    uint16_t rpm;

    tDiffZeroToZeroAvg = (DiffZerotoZero + PrevDiffZerotoZero) >> 1;

    if( *bFirstCrankPulse == true)
    {
        rpm = 150;
        *bFirstCrankPulse = false;
    }
    else
    {
       rpm = (uint16_t)((uint32_t)(RPM_CALC_FACTOR/IgnTimerResolution) / tDiffZeroToZeroAvg);
    }
    return rpm;
}


/*********************************************************************************************************************************************
function          : CheckEngineCranked
description       : This function checks whether engine is cranked and sets/clear the flag
Input parameters  : Engine speed, engine cranked rpm and nos of engine cranked cycles.
Output parameters : EngineCranked flag
Return value      : None
**********************************************************************************************************************************************/
void CheckEngineCranked(uint16_t Engine_Rpm, uint16_t EngineCranked_Rpm, bool *EngineCranked, uint8_t nEngineCrankCycles)
{
    if(*EngineCranked == false)
    {
        if(Engine_Rpm >= EngineCranked_Rpm)
        {
            nRpmCrossedCrankRpm++;
        }
        else
        {
            nRpmCrossedCrankRpm = 0;
        }
        if(nRpmCrossedCrankRpm >= nEngineCrankCycles)
        {
            *EngineCranked = true;
            nRpmCrossedCrankRpm = 0;
        }
    }
    else
    {
        if(Engine_Rpm < EngineCranked_Rpm)
        {
            *EngineCranked = false;
        }
    }
}


/*********************************************************************************************************************************************
function          : CutOffIgnition
description       : This function is used to set/clear SparkCutoff flag if RPM is more than cutoff Rpm
Input parameters  : engine speed, spark cutoff RPM, resume RPM, cutoffspark flag and number of cutoff cycles.
Output parameters : cutoffSpark flag
Return value      : None
**********************************************************************************************************************************************/
void CutOffIgnition(uint16_t rpm, uint16_t SparkCuoffRpm, uint16_t SparkResumeRpm, bool *bCutoffSpark, uint8_t nCutoffCycles)
{
    static uint8_t nRpmCrossedSparkCutoffRpm;

    if( *bCutoffSpark == false)
    {
        if(rpm > SparkCuoffRpm)
        {
            nRpmCrossedSparkCutoffRpm++;
            if(nRpmCrossedSparkCutoffRpm >= nCutoffCycles)
            {
                nRpmCrossedSparkCutoffRpm = nCutoffCycles + 1;
                *bCutoffSpark = true;
            }
        }
        else
        {
            nRpmCrossedSparkCutoffRpm = 0;
        }
    }
    else
    {
        if(rpm < SparkResumeRpm)
        {
            nRpmCrossedSparkCutoffRpm = 0;
            *bCutoffSpark = false;
        }
    }
}


/*********************************************************************************************************************************************
function          : ComputeAdvToIgnTime
description       : This function is used to compute Ignition scheduling time from Advance pulse
Input parameters  : Ignition timing, reference strip time to be used for computation and hardware
                    delay count
Output parameters : None
Return value      : Ignition scheduling time from advance pulse
**********************************************************************************************************************************************/
uint16_t ComputeAdvToIgnTime(int16_t IgnTiming_deg, uint32_t StripRefTime, uint8_t HardwareOffset)
{
    uint16_t tIgnFromAdv = 0;

    tIgnFromAdv = (uint16_t)(((uint32_t)((CAL_PIP_WIDTH_DEG + CAL_PIP_BASE_DEG) - IgnTiming_deg) * StripRefTime) / CAL_PIP_WIDTH_DEG);

    if(tIgnFromAdv > HardwareOffset)
    {
        tIgnFromAdv = tIgnFromAdv - HardwareOffset;
    }
    else
    {
        tIgnFromAdv = HardwareOffset;
    }
    return tIgnFromAdv;
}


/*********************************************************************************************************************************************
function          : ComputeDwellStartTime
description       : This function is used to compute dwell start
Input parameters  : Dwell end at zero pulse flag, Ignition scheduling time form advance pulse,
                    dwell time, zero to zero and advance to zero time reference
Output parameters : None
Return value      : Dwell start time from zero pulse
***********************************************************************************************************************************************/
uint16_t ComputeDwellStartTime(bool bDwellEndAtZero, uint16_t TimeAdvToIgn, uint32_t DiffZerotoZero, uint32_t DiffAdvToZero, uint16_t DwellTime )
{
    uint32_t TimeZeroToDwellStart = 0;

    if(!bDwellEndAtZero)
    {
        TimeZeroToDwellStart = ((DiffZerotoZero - DiffAdvToZero) + TimeAdvToIgn) - DwellTime;
    }
    else
    {
        TimeZeroToDwellStart = DiffZerotoZero - DwellTime;
    }

    if(TimeZeroToDwellStart >= 65535)
    {
        TimeZeroToDwellStart = 65535;
    }
    return (uint16_t)TimeZeroToDwellStart;
}



