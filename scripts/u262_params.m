clear;
SimMode = false;

J = 10e-3; % 3 milli kg.m^2

% Machine specifications
Ld = 63.5e-6; % Henry
Lq = 85.5e-6; % Henry
R = 50e-3; % Ohm
P = 6;      % number of pole pairs
BemfConst = 0.029; % V/rad

Torque_Load_K = 0.001; % load torque per RPM

R_Bat = 0;
Voc_Bat = 12;
PM_Flux =0.00486;
% PM_Flux = (BemfConst*60)/(2*pi*sqrt(3)*P);

ConstSpeedMode = 0;
ConstSpeed_RPM = 10;

BHspin_SamplingRatio = 0.2;
BHspin_Ts = 100e-6;
HallPhaseOffset = 0;