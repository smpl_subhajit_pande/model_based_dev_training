% Module       : get_motoring_tables.m
% Author(s)    : Samyak Kamble, Anay Joshi
% Date created : 11th July 2019
% Last updated : 11th July 2019
% Description  : This function takes as input a config dictionary which
%                contains values of machine characteristics & constraints
%                such as voltage & current limits, and takes speed of motor
%                as input. The function returns maximum possible assist
%                torque (in Nm) that is possible to be generated at the
%                given speed using the given machine + constraints.
function [Tmax] = get_Tmax(Speed_RPM, config)

Imax_RMS = config.Imax_RMS; 
U_AMP_MAX = config.U_AMP_MAX;
U_THETA_STEP = config.U_AMP_STEP;
U_AMP_STEP = config.U_AMP_STEP;

Torque_list = NaN(length(0:U_THETA_STEP:360), ...
                         length(0:U_AMP_STEP:U_AMP_MAX));
idx = 1;        
for Utheta = 0:U_THETA_STEP:360
    idy = 1;
    for Uamp = 0:U_AMP_STEP:U_AMP_MAX
        
        [I_RMS, Torque] = op_point(Uamp, Utheta, Speed_RPM, config);
        
        if abs(I_RMS) <= Imax_RMS                
            Torque_list(idx, idy) = Torque;
        end            
        idy = idy + 1;
    end
    idx = idx + 1;
end

Tmax = max(max(Torque_list));

end