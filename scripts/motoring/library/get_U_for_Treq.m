% Module       : get_U_for_Treq.m
% Author(s)    : Anay Joshi, Samyak Kamble
% Date created : 11th July 2019
% Last updated : 11th July 2019
% Description  : This function takes as input a config dictionary (which
%                contains values of machine characteristics & constraints
%                such as voltage & current limits), speed of motor &
%                required torque (in Nm) as input. 
%                The function returns the value of U_amp & U_theta
%                to be applied by the controller to get required
%                torque at the given speed. Since the function is an
%                approximation, it also returns the value of torque that
%                will get applied to the machine (this value will typically
%                be close to the input argument Treq)
function [Utheta, Uamp, Torque] = get_U_for_Treq(Speed_RPM, Treq, config)

U_AMP_MAX = config.U_AMP_MAX;
U_THETA_STEP = config.U_AMP_STEP;
U_AMP_STEP = config.U_AMP_STEP;

Iline_list = NaN(length(0:U_THETA_STEP:360), ...
                 length(0:U_AMP_STEP:U_AMP_MAX));
             
Torque_list = NaN(length(0:U_THETA_STEP:360), ...
    length(0:U_AMP_STEP:U_AMP_MAX));

idx = 1;
for Utheta = 0:U_THETA_STEP:360
    idy = 1;
    for Uamp = 0:U_AMP_STEP:U_AMP_MAX
        
        [I_RMS, Torque] = op_point(Uamp, Utheta, Speed_RPM, config);
        
        if (abs(Torque - Treq) < max([0.05 Treq*0.05]))
            Iline_list(idx, idy) = I_RMS;
            Torque_list(idx, idy) = Torque;
        end
        idy = idy + 1;
    end
    idx = idx + 1;
end

I_RMS_min = min(min(abs(Iline_list)));
min_lin_index = find(abs(Iline_list) == I_RMS_min);

if isempty(min_lin_index)
    Utheta = -1;
    Uamp = -1;
else
    min_lin_index = min_lin_index(1);
    [min_idx, min_idy] = ind2sub(size(Iline_list), min_lin_index);
    % min_lin_index == (y-1)*length(0:U_THETA_STEP:360) + x
    Utheta = (min_idx - 1)*U_THETA_STEP;
    Uamp = (min_idy - 1)*U_AMP_STEP;
    Torque = Torque_list(min_idx, min_idy);
end
