% Module       : generate_motoring_mat_file.m
% Author(s)    : Anay Joshi, Samyak Kamble
% Date created : 18th July 2019
% Last updated : 18th July 2019
% Description  : This module outputs mat file which can be imported in
%                data dictionary under "Motoring" section. User of
%                this file needs to select the machine type (and populate 
%                machine characteristics & voltage/current limits) & also
%                mention whether "truncated" mat file is to be generated.
%                Truncated mode will neglect the effect of Vbat and use
%                same calibration tables for each Vbat.
clear;                            

truncate = true;   % Truncate == true will create same tables for all Vbats
machine = 'u262';

config = {};
config.SPEED_RANGE = horzcat(600:200:2000, 2500:500:10000);
config.TORQUE_PCT_RANGE = 0:10:100;

if strcmp(machine, 'u262') == 1
    config.Imax_RMS = 45;      % line RMS current
    config.BemfConst = 4.9;    % Bemf amplitude at 1000 RPM (not RMS)
    config.PP = 6;             % Number of pole-pairs
    config.R = 0.042;          % Stator resistance per phase (assuming star) (Ohm)
    config.I_sat = 100;        % Saturation current of stator material (Amp)
    config.R_mag = 70;         % Contribution of rotor field in stator saturation (Amp)
    config.L = 81e-6;          % Stator inductance per phase (assuming star) (Henry)
    config.U_AMP_MAX = 95;     % Between 0 to 100
    config.U_THETA_STEP = 1;   % Degrees
    config.U_AMP_STEP = 1;     % Duty
    
elseif strcmp(machine, '1a') == 1
    config.Imax_RMS = 60;      % line RMS current
    config.BemfConst = 4.5;    % Bemf amplitude at 1000 RPM (not RMS)
    config.PP = 6;             % Number of pole-pairs
    config.R = 0.022;          % Stator resistance per phase (assuming star) (Ohm)
    config.I_sat = 100;        % Saturation current of stator material (Amp)
    config.R_mag = 70;         % Contribution of rotor field in stator saturation (Amp)
    config.L = 50.5e-6;        % Stator inductance per phase (assuming star) (Henry)
    config.U_AMP_MAX = 80;     % Between 0 to 100
    config.U_THETA_STEP = 1;   % Degrees
    config.U_AMP_STEP = 1;     % Duty
end

CAL_MOTORING_U_THETA_x = uint16(config.SPEED_RANGE);
CAL_MOTORING_U_THETA_y = uint8(config.TORQUE_PCT_RANGE);
CAL_MOTORING_U_AMP_x = uint16(config.SPEED_RANGE);
CAL_MOTORING_U_AMP_y = uint8(config.TORQUE_PCT_RANGE);
CAL_MOTORING_TABLE_SPEED_AXIS_LENGTH = uint8(length(config.SPEED_RANGE));
CAL_MOTORING_TABLE_TORQUE_AXIS_LENGTH = uint8(length(config.TORQUE_PCT_RANGE));


if truncate == false
    
    config.Vbat = 8.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_8000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_8000mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 8.5;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_8500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_8500mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 9.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_9000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_9000mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 9.5;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_9500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_9500mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 10.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_10000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_10000mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 10.5;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_10500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_10500mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 11.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_11000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_11000mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 11.5;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_11500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_11500mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 12.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_12000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_12000mV_U_AMP_z = uint8(U_AMP_z);

    config.Vbat = 12.5;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);    
    CAL_MOTORING_12500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_12500mV_U_AMP_z = uint8(U_AMP_z);
    
else
    
    config.Vbat = 12.0;
    [U_THETA_z, U_AMP_z] = get_motoring_tables(config);
    CAL_MOTORING_8000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_8000mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_8500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_8500mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_9000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_9000mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_9500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_9500mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_10000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_10000mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_10500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_10500mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_11000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_11000mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_11500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_11500mV_U_AMP_z = uint8(U_AMP_z);
    CAL_MOTORING_12000mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_12000mV_U_AMP_z = uint8(U_AMP_z);  
    CAL_MOTORING_12500mV_U_THETA_z = uint16(U_THETA_z);
    CAL_MOTORING_12500mV_U_AMP_z = uint8(U_AMP_z);
end

save('motoring.mat', 'CAL_MOTORING_8000mV_U_THETA_z', ...
                   'CAL_MOTORING_8500mV_U_THETA_z', ...
                   'CAL_MOTORING_9000mV_U_THETA_z', ...
                   'CAL_MOTORING_9500mV_U_THETA_z', ...
                   'CAL_MOTORING_10000mV_U_THETA_z', ...
                   'CAL_MOTORING_10500mV_U_THETA_z', ...
                   'CAL_MOTORING_11000mV_U_THETA_z', ...
                   'CAL_MOTORING_11500mV_U_THETA_z', ...
                   'CAL_MOTORING_12000mV_U_THETA_z', ...
                   'CAL_MOTORING_12500mV_U_THETA_z', ...
                   'CAL_MOTORING_8000mV_U_AMP_z', ...
                   'CAL_MOTORING_8500mV_U_AMP_z', ...
                   'CAL_MOTORING_9000mV_U_AMP_z', ...
                   'CAL_MOTORING_9500mV_U_AMP_z', ...
                   'CAL_MOTORING_10000mV_U_AMP_z', ...
                   'CAL_MOTORING_10500mV_U_AMP_z', ...
                   'CAL_MOTORING_11000mV_U_AMP_z', ...
                   'CAL_MOTORING_11500mV_U_AMP_z', ...
                   'CAL_MOTORING_12000mV_U_AMP_z', ...
                   'CAL_MOTORING_12500mV_U_AMP_z', ...
                   'CAL_MOTORING_U_THETA_x', ...
                   'CAL_MOTORING_U_THETA_y', ...
                   'CAL_MOTORING_U_AMP_x', ...
                   'CAL_MOTORING_U_AMP_y', ...
                   'CAL_MOTORING_TABLE_SPEED_AXIS_LENGTH', ...
                   'CAL_MOTORING_TABLE_TORQUE_AXIS_LENGTH');